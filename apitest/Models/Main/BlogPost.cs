﻿using System.ComponentModel.DataAnnotations.Schema;

namespace apitest.Models.Main
{
    public class BlogPost
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int Id { get; set; }
            public string Title { get; set; }
            public string Content { get; set; }
            public string ImagePath { get; set; }

        [NotMapped]
            public IFormFile File { get; set; }
         
 
    }
}
