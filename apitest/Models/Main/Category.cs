﻿namespace apitest.Models.Main
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UrlHandle { get; set; }
    }
}
