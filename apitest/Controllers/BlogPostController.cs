﻿using apitest.Data;
using apitest.Models.Main;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace apitest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogPostController : ControllerBase
    {
        private readonly ApiDbContext apiDbContext;

        public BlogPostController(ApiDbContext apiDbContext)
        {
            this.apiDbContext = apiDbContext;
        }
 
        [HttpPost]
        public async Task<ActionResult<List<BlogPost>>> AddBlogPost([FromForm] BlogPost formData)
        {
            if (formData != null)
            {
               
                if (formData.File != null)
                {
            
                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(formData.File.FileName);
                    var filePath = Path.Combine(Directory.GetCurrentDirectory(), "uploads", fileName);

                 
                    Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "uploads"));

             
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await formData.File.CopyToAsync(stream);
                    }

                    formData.ImagePath = filePath; 
                }
                 
                var blogPost = new BlogPost
                {
                    Title = formData.Title,
                    Content = formData.Content,
                    ImagePath = formData.ImagePath  
                };

                apiDbContext.BlogPosts.Add(blogPost);
                await apiDbContext.SaveChangesAsync();

                return Ok(await apiDbContext.BlogPosts.ToListAsync());
            }

            return BadRequest("Object instance not set");
        }



        [HttpGet]
        public async Task<ActionResult<List<BlogPost>>> GetBlogPostAll()
        {
            var a = await apiDbContext.BlogPosts.ToListAsync();
            return Ok(a);
        }
        
        [HttpGet("{id:int}")]
        public async Task<ActionResult<List<BlogPost>>> GetBlogPost(int id)
        {
            var a = await apiDbContext.BlogPosts.FirstOrDefaultAsync(e => e.Id == id);
            return Ok(a);
        }

        [HttpPut]
        public async Task<ActionResult<BlogPost>> UpdateBlogPost(BlogPost post)
        {
            if (post != null)
            {
                var a = await apiDbContext.BlogPosts.FirstOrDefaultAsync(e => e.Id == post.Id);
          
                await apiDbContext.SaveChangesAsync();  

                return Ok(a);
            }

            return BadRequest("not found");
        }

        [HttpDelete]
        public async Task<ActionResult<BlogPost>> DeleteBlogPost(BlogPost post)
        {
            var a = await apiDbContext.BlogPosts.FirstOrDefaultAsync(e => e.Id == post.Id);
            if (post != null)
            {
                apiDbContext.BlogPosts.Remove(a);
                await apiDbContext.SaveChangesAsync();
  
                return Ok(await apiDbContext.BlogPosts.ToListAsync());
            }

            return NotFound();
        }


    }
}
